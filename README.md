# `wordexp-sys`

Rust FFI bindings for the `wordexp(3)` C function.

# See Also

* [`cromulent`](https://crates.io/crates/cromulent): The safe wrapper around these bindings.
* [`wordexp`](https://crates.io/crates/wordexp): A `wordexp` wrapper which does _not_ use these bindings.
