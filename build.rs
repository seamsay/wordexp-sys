extern crate bindgen;

fn main() {
    println!("cargo:rerun-if-changed=wrapper.h");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .allowlist_type("wordexp_t")
        .allowlist_function("wordexp")
        .allowlist_function("wordfree")
        .allowlist_var("WRDE_DOOFFS")
        .allowlist_var("WRDE_APPEND")
        .allowlist_var("WRDE_NOCMD")
        .allowlist_var("WRDE_REUSE")
        .allowlist_var("WRDE_SHOWERR")
        .allowlist_var("WRDE_UNDEF")
        .allowlist_var("WRDE_NOSPACE")
        .allowlist_var("WRDE_BADCHAR")
        .allowlist_var("WRDE_BADVAL")
        .allowlist_var("WRDE_CMDSUB")
        .allowlist_var("WRDE_SYNTAX")
        .generate()
        .unwrap();

    let path = std::path::PathBuf::from(std::env::var("OUT_DIR").unwrap());
    bindings.write_to_file(path.join("bindings.rs")).unwrap();
}
