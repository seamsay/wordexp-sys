use wordexp_sys::*;

#[test]
fn sanity() {
    let mut result = wordexp_t {
        we_wordc: 0,
        we_wordv: std::ptr::null_mut(),
        we_offs: 0,
    };

    let to_expand = std::ffi::CString::new("*").unwrap();
    let error = unsafe { wordexp(to_expand.as_ptr(), &mut result, 0) };
    assert_eq!(error, 0);

    assert_eq!(result.we_wordc, 9);
    assert_eq!(result.we_offs, 0);
    assert!(!result.we_wordv.is_null());

    let mut found = std::collections::HashSet::new();
    for i in 0..result.we_wordc {
        let word = unsafe { *result.we_wordv.offset(i.try_into().unwrap()) };
        assert!(!word.is_null());
        let word = unsafe { std::ffi::CStr::from_ptr(word) };
        let word = word.to_str().unwrap();
        found.insert(word);
    }

    let expected = std::collections::HashSet::from([
        "build.rs",
        "Cargo.lock",
        "Cargo.toml",
        "flake.lock",
        "flake.nix",
        "src",
        "target",
        "tests",
        "wrapper.h",
    ]);

    assert_eq!(found, expected);
}
