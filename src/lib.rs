#[allow(non_camel_case_types)]
#[allow(non_upper_case_globals)]
#[cfg_attr(test, allow(deref_nullptr))]
mod bindings {
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

pub use bindings::*;
